import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonTextAvatar } from 'ionic-text-avatar';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';

//AngularModules
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgCalendarModule  } from 'ionic2-calendar';

import { MyApp } from './app.component';
import { CalendarPage } from '../pages/calendar/calendar';
import { HomePage } from '../pages/home/home';
import { HomePopoverPage } from '../pages/home-popover/home-popover';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { MessagePage } from '../pages/message/message';
import { NewGroupModalPage } from '../pages/new-group-modal/new-group-modal';
import { NewListModalPage } from '../pages/new-list-modal/new-list-modal';
import { NotificationPage } from '../pages/notification/notification';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { SearchPage } from '../pages/search/search';
import { SettingPage } from '../pages/setting/setting';
import { TabsPage } from '../pages/tabs/tabs';
import { TaskPage } from '../pages/task/task';

import { RestProvider } from '../providers/rest/rest';

@NgModule({
  declarations: [
    MyApp,
    CalendarPage,
    HomePage,
    HomePopoverPage,
    ListPage,
    LoginPage,
    MessagePage,
    NewGroupModalPage,
    NewListModalPage,
    NotificationPage,
    ProfilePage,
    RegisterPage,
    SearchPage,
    SettingPage,
    TabsPage,
    TaskPage,
    IonTextAvatar
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    NgCalendarModule,
    IonicModule.forRoot(MyApp, {tabsPlacement: 'bottom',tabsHideOnSubPages: true})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CalendarPage,
    HomePage,
    HomePopoverPage,
    ListPage,
    LoginPage,
    MessagePage,
    NewGroupModalPage,
    NewListModalPage,
    NotificationPage,
    ProfilePage,
    RegisterPage,
    SearchPage,
    SettingPage,
    TabsPage,
    TaskPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider,
    FileTransfer,
    File,
    Camera
  ]
})
export class AppModule {}

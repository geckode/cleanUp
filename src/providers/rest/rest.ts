import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class RestProvider {

	// apiUrl = 'https://www.keenonklean.com/api/v1/';
	//apiUrl = 'http://localhost/kok/public/api/v1/';
	apiUrl = 'http://localhost:8200/api/';

	headers:any;

  	constructor(public http: HttpClient) {
  		this.headers = new Headers();
  	}

  	getData(type) {
		return new Promise((resolve, reject) => {
	    	this.http.get(this.apiUrl+type, {headers: this.headers})
	    	.subscribe(data => {
	      		resolve(data);
	    	}, (err) => {
	      		reject(err);
	    	});
	  	});
	}

	postData(credentials, type) {
		return new Promise((resolve, reject) => {
		    this.http.post(this.apiUrl+type, JSON.stringify(credentials), {headers: this.headers})
		    .subscribe(res => {
		        resolve(res);
	      	}, (err) => {
		        reject(err);
		    });
		});
	}
}

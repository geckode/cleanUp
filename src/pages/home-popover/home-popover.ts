import { Component } from '@angular/core';
import { IonicPage, ViewController, ModalController } from 'ionic-angular';

import { NewListModalPage } from '../new-list-modal/new-list-modal';

@IonicPage()
@Component({
  selector: 'page-home-popover',
  templateUrl: 'home-popover.html',
})
export class HomePopoverPage {

	public user: any;
	
  	constructor(public viewCtrl: ViewController, public modalCtrl: ModalController,) 
  	{
  		const data = JSON.parse(localStorage.getItem('userData'));
    	this.user = data;
  	}

  	public close() 
  	{
    	this.viewCtrl.dismiss();
  	}

  	public showNewListModal()
  	{	
  		var id = this.user.id;
  		let modal = this.modalCtrl.create(NewListModalPage,{idUser: id});
    	modal.present();
  	}

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { TaskPage } from '../task/task';

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})	
export class ListPage {

	public user:any;
	public list:any;
	public tasks:any;
	public task = {"task":"","idList":""};
	public taskStatus = {"id_task":"","status":""};
	public response:any;

  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public restService: RestProvider,
  		public loadingCtrl: LoadingController,
  		public toastCtrl: ToastController)
  	{
  		const data = JSON.parse(localStorage.getItem('userData'));
    	this.user = data;
    	this.list = this.navParams.data;
    	this.task.idList = this.list.idList;
  		this.getTasksByList();
  	}

  	public goToTask(id_task, taskName) 
  	{	
		this.navCtrl.push(TaskPage, {id_task: id_task, name: taskName});
	}

  	public getTasksByList() 
  	{	
  		let loader = this.loadingCtrl.create({
      		content: "Wait..."
		});	
		console.log(this.list);
    	loader.present();
    	this.task.task = null;
	  	this.restService.getData('get-tasks-by-list/' + this.list.id_list)
	      .then((result)=>{
	        this.response = result;
	        if (this.response.error) {
	        	loader.dismiss();
	          	this.presentToast(this.response.text);
	        } else {
	        	loader.dismiss();
	          	this.tasks = this.response.tasks;
	          	this.list = this.response.list;
	        }
	    });
	}

	public createNewTask() 
	{	
	  	this.restService.postData(this.task, 'new-task')
	      .then((result)=>{
	        this.response = result;
	        if (this.response.error){
	          	this.presentToast(this.response.text);
	        } else {
	        	this.getTasksByList();
	        }
	      });
	}

	public deleteTask(idTask)
	{
		this.restService.getData('destroy-task/' + idTask)
	      	.then((result)=>{
	      		this.response = result;
		        if (this.response.error){
		          	this.presentToast(this.response.text);
		        } else {
		        	this.getTasksByList();
		        }
	    });
	}

	public doRefresh(refresher) {
	    this.getTasksByList();
	    setTimeout(() => {
	      console.log('Async operation has ended');
	      refresher.complete();
	    }, 1000);
  	}

  	public presentToast(msg) 
  	{
	    let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 3000,
	      position: 'bottom'
	    });

	    toast.onDidDismiss(() => {
	      console.log('Dismissed toast');
	    });

	    toast.present();
  	}

  	public toogleStatusTask(id_task, status)
  	{	
  		this.taskStatus.id_task = id_task;
  		this.taskStatus.status = status;
  		this.restService.postData(this.taskStatus, 'update-task-status')
      	.then((result)=>{
	        this.response = result;
	        if (this.response.error){
	          	this.presentToast(this.response.text);
	        } else {
	        	this.getTasksByList();
	        }
      	});
  	}

}

import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';

import {
  RestProvider
} from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  public searchInput: any;
  public response: any;
  public results: any;
  public user: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public toastCtrl: ToastController) {
    const data = JSON.parse(localStorage.getItem('userData'));
    this.user = data;
  }

  public onInput(ev: any) {
    const val = ev.target.value;
    this.restService.getData('get-all/' + this.user.id + '/' + val)
      .then((result) => {
        this.response = result;
        if (this.response.error) {
          this.presentToast(this.response.text);
        } else {
          this.results = this.response.data;
        }
      });
  }

  public onCancel(event) {
    this.searchInput = null;
    console.log('Lo cancelo');
  }

  public presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
}

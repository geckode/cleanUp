import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController
} from 'ionic-angular';

import {
  TabsPage
} from '../tabs/tabs';
import {
  RegisterPage
} from '../register/register';
import {
  RestProvider
} from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public response: any;
  public user = {
    "email": "",
    "password": ""
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) {
    if (localStorage.getItem('userData')) {
      this.navCtrl.setRoot(TabsPage);
    }
  }

  public login() {
    let loader = this.loadingCtrl.create({
      content: "Wait..."
    });
    loader.present();
    this.restService.postData(this.user, 'login')
      .then((result) => {
        this.response = result;
        if (this.response.error) {
          loader.dismiss();
          this.presentToast(this.response.text);
        } else {
          localStorage.setItem('userData', JSON.stringify(this.response.userData));
          loader.dismiss();
          this.navCtrl.setRoot(TabsPage);
        }
      }, (err) => {});
  }

  public presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  public goToRegister() {
    this.navCtrl.push(RegisterPage);
  }

}

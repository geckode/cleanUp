import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewGroupModalPage } from './new-group-modal';

@NgModule({
  declarations: [
    NewGroupModalPage,
  ],
  imports: [
    IonicPageModule.forChild(NewGroupModalPage),
  ],
})
export class NewGroupModalPageModule {}

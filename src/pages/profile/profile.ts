import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Slides } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
	@ViewChild(Slides) slides: Slides;
	public user:any;
	public response:any;
	public initialSlide:any;
	public userToUpdate = {"name":"","email":"","color":"","id":""};
	public colors = {0:"blue",1:"purple",2:"red",3:"green",4:"yellow"};

  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public loadingCtrl: LoadingController,
    	public restService: RestProvider,
    	public toastCtrl: ToastController) 
  	{
  		const data = JSON.parse(localStorage.getItem('userData'));
    	this.user = data;
    	var objectToarray = ['blue','purple','red','green','yellow'];
		  this.initialSlide = objectToarray.indexOf(this.user.iconColor);
		  console.log(this.initialSlide);
    	this.userToUpdate.id = this.user.id;
    	this.userToUpdate.name = this.user.name;
    	this.userToUpdate.email = this.user.email;
    	this.userToUpdate.color = this.user.iconColor;
  	}

  	public updateUser()
  	{	
  		let loader = this.loadingCtrl.create({
	    	content: "Wait..."
	    });
	    loader.present();
	  	this.restService.postData(this.userToUpdate, 'update-user')
      		.then((result)=>{
        this.response = result;
        	if (this.response.error){
          		loader.dismiss();
            	this.presentToast(this.response.text);
        	} else {
          		loader.dismiss();
          		localStorage.setItem('userData', JSON.stringify(this.response.userData));
        		this.navCtrl.setRoot(TabsPage);
        	}
      	});
  	}

  	public presentToast(msg) 
	{
	    let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 3000,
	      position: 'bottom'
	    });

	    toast.onDidDismiss(() => {
	      console.log('Dismissed toast');
	    });

	    toast.present();
	}

	public slideChanged() 
	{
    	let currentIndex = this.slides.getActiveIndex();
    	this.userToUpdate.color = this.colors[currentIndex];
  	}
}

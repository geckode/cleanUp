import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { TaskPage } from '../task/task';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {

	public user:any;
	public response:any;
	public messages:any;

  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public restService: RestProvider,
      public loadingCtrl: LoadingController,
  		public toastCtrl: ToastController)
  	{
  		const data = JSON.parse(localStorage.getItem('userData'));
    	this.user = data;
    	this.getMessages();
  	}

  	public getMessages()
  	{
  		let loader = this.loadingCtrl.create({
      		content: "Wait..."
    	});
    	loader.present();
	  	this.restService.getData('get-messages-by-user/' + this.user.id)
	      .then((result)=>{
	        this.response = result;
	        if (this.response.error) {
	        	loader.dismiss();
	          	this.presentToast(this.response.text);
	        } else {
	        	loader.dismiss();
	          	this.messages = this.response.messages;
	        }
	    });
  	}

  	public presentToast(msg) 
  	{
	    let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 3000,
	      position: 'bottom'
	    });

	    toast.onDidDismiss(() => {
	      console.log('Dismissed toast');
	    });

	    toast.present();
  	}

    public goToTask(idUser, idMessage, taskName, idTask)
    {
      let loader = this.loadingCtrl.create({
          content: "Wait..."
      });
      loader.present();
      this.restService.getData('update-user-message-status/' + idUser + '/' + idMessage)
        .then((result)=>{
          this.response = result;
          if (this.response.error) {
            loader.dismiss();
              this.presentToast(this.response.text);
          } else {
            loader.dismiss();
              this.navCtrl.push(TaskPage, {idTask: idTask, name: taskName});
          }
      });
    }
}

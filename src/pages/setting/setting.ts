import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { ProfilePage } from '../profile/profile';

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {


  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public app: App) {
  	}

  	public goToEditProfile()
  	{
  		this.navCtrl.push(ProfilePage);
  	}

  	public logOut(){
    	localStorage.clear();
    	this.app.getRootNav().setRoot(LoginPage);
  		// this.navCtrl.setRoot(LoginPage);
  	}

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { MessagePage } from '../message/message';
import { NotificationPage } from '../notification/notification';
import { SearchPage } from '../search/search';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-tabs'	,
  templateUrl: 'tabs.html',
})
export class TabsPage {

	tab1 = HomePage;
	tab2 = MessagePage;
	tab3 = NotificationPage;
	tab4 = SearchPage;
	public user:any;
	public notReadMessages:any;
	public response:any;
	public quantityMsg:any;
	public quantityNtf:any;

  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public restService: RestProvider,
  		public toastCtrl: ToastController) {
  		const data = JSON.parse(localStorage.getItem('userData'));
    	this.user = data;
  		this.getNotReadMessages();
  	}

  	public getNotReadMessages()
  	{
  		this.restService.getData('get-not-read-quantity-messages/' + this.user.id)
	      	.then((result)=>{
	        	this.response = result;
	        	if (this.response.error){
	          		this.presentToast(this.response.text);
	        	} else {
	          		this.quantityMsg = this.response.quantityMsg;
	          		this.getNotReadNotifications();
	        	}
	    	});
  	}

  	public getNotReadNotifications()
  	{
  		this.restService.getData('get-not-read-quantity-notifications/' + this.user.id)
	      	.then((result)=>{
	        	this.response = result;
	        	if (this.response.error){
	          		this.presentToast(this.response.text);
	        	} else {
	          		this.quantityNtf = this.response.quantityNtf;
	        	}
	    	});
  	}

  	public presentToast(msg) 
	{
	    let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 3000,
	      position: 'bottom'
	    });

	    toast.onDidDismiss(() => {
	      console.log('Dismissed toast');
	    });

	    toast.present();
	}

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
import { RestProvider } from '../../providers/rest/rest';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
*  Soy desarrollador fullstack de webapps reactivas para experiencias de usuario exquisitas, pienso que es mejor el minimalismo acompañado de una buena herramienta eficiente para mejorar el trabajo de cualquiera.
*  Para desarrollo web trabajo excelente con PHP, Laravel, Javascript, NodeJs, VueJs, Typescript y Angular.
* En desarrollo movil me he experimentado bastante en Ionic
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

	public user = {"name":"","email":"","password":""};
	public response:any;

  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public restService: RestProvider,
    	public loadingCtrl: LoadingController,
    	public toastCtrl: ToastController)
    {
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad RegisterPage');
  	}

  	public register()
  	{
  		let loader = this.loadingCtrl.create({
	    	content: "Wait..."
	    });
	    loader.present();
	  	this.restService.postData(this.user, 'register')
      		.then((result)=>{
        this.response = result;
        	if (this.response.error){
          		loader.dismiss();
            	this.presentToast(this.response.text);
        	} else {
          		localStorage.setItem('userData', JSON.stringify(this.response.userData));
          		loader.dismiss();
          		this.navCtrl.setRoot(TabsPage);
        	}
      	});
	}

	public presentToast(msg) 
  	{
	    let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 3000,
	      position: 'bottom'
	    });
	    toast.onDidDismiss(() => {
	      console.log('Dismissed toast');
	    });
	    toast.present();
  	}

}

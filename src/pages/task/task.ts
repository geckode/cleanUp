import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, LoadingController, ToastController  } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-task',
  templateUrl: 'task.html',
})
export class TaskPage {

  	public user:any;
  	public task:any;
  	public subtasks:any;
  	public newSubtask:any;
  	public imageURI:any;
  	public imageFileName:any;
  	public response:any;
    public msg = {"idUser":"","id_task":"","text":""};
    public messages:any;
    public messageInput:any;
  	public subtaskStatus = {"idSubtask":"","status":""};
  	public updTask = {"id_task":"","name":"","expirationDate":"","rememberDate":"","repeat":"","note":"", "subtasks":""};

  	constructor(
    	public navCtrl: NavController, 
    	public navParams: NavParams,
    	private transfer: FileTransfer,
    	private camera: Camera,
    	public loadingCtrl: LoadingController,
    	public toastCtrl: ToastController,
    	public actionSheetCtrl: ActionSheetController,
    	public restService: RestProvider) 
  	{
    	const data = JSON.parse(localStorage.getItem('userData'));
    	this.user = data;
    	this.task = this.navParams.data;
    	this.getTask();
      this.getMessages();
  	}

  	public addSubtask()
  	{	
  		if (typeof this.newSubtask === 'undefined'){
  			this.presentToast("Subtask cant be empty");
  		} else {
    		this.subtasks.push(this.newSubtask);
    		console.log(this.subtasks);
    		this.newSubtask = null;	
  		}
  	}

  public getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.uploadFile();
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }

  public uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'image',
      fileName: 'task',
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }

    fileTransfer.upload(this.imageURI, 'https://www.keenonklean.com/api/v1/upload-task-image', options)
      .then((data) => {
        this.imageFileName = "https://www.keenonklean.com/path/" + data.response;
        loader.dismiss();
        this.presentToast("Image uploaded successfully");
    }, (err) => {
      loader.dismiss();
      this.presentToast(err.source);
    });
  }

  public presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

   public presentActionSheet() {
   let actionSheet = this.actionSheetCtrl.create({
     title: 'Add a File',
     buttons: [
       {
         text: 'Galery',
         handler: () => {
           this.getImage();
         }
       },
       {
         text: 'Camera',
         handler: () => {
           console.log('Archive clicked');
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });

   actionSheet.present();
  }

  	public updateTask(id_task)
  	{	
  		let loader = this.loadingCtrl.create({
      		content: "Uploading..."
    	});
    	loader.present();
  		this.updTask.id_task = id_task;
  		this.updTask.subtasks = this.subtasks;
	  	this.restService.postData(this.updTask, 'update-task')
	    .then((result)=>{
	    	this.response = result;
        	loader.dismiss();
          	this.presentToast(this.response.text);
		  });
  	}

  	public getTask()
  	{	
  		let loader = this.loadingCtrl.create({
      		content: "Uploading..."
    	});
    	loader.present();
  		this.restService.getData('get-task/' + this.task.id_task)
	      .then((result)=>{
	        this.response = result;
	        if (this.response.error) {
	          	loader.dismiss();
          		this.presentToast(this.response.text);
	        } else {
	        	this.getSubtasks();
	        	loader.dismiss();
	          	this.updTask = this.response.task;
	        }
	    });
  	}

  	public getSubtasks()
  	{
		  this.restService.getData('get-subtasks/' + this.task.id_task)
	      .then((result)=>{
	        this.response = result;
	        if (this.response.error) {
          		this.presentToast(this.response.text);
	        } else {
	          	this.subtasks = this.response.subtasks;
	        }
	    });
  	}



  	public deleteSubtask(idSubtask)
  	{
		console.log(idSubtask);
  	}

  	public toogleStatusSubtask(idSubtask, status)
  	{
  		this.subtaskStatus.idSubtask = idSubtask;
  		this.subtaskStatus.status = status;
  		this.restService.postData(this.subtaskStatus, 'update-subtask-status')
      	.then((result)=>{
	        this.response = result;
	        if (this.response.error){
	          	this.presentToast(this.response.text);
	        } else {
	        	// this.getTasksByList();
	        }
      	});
  	}

    public handleMessage()
    { 
      this.msg.idUser = this.user.id;
      this.msg.id_task = this.task.id_task;
      this.msg.text = this.messageInput;
      this.restService.postData(this.msg, 'new-task-message')
      .then((result)=>{
        this.response = result;
        if (this.response.error){
          this.presentToast(this.response.text);
        } else {
          this.messageInput = null;
          this.getMessages();
        }
      });
    }

    public getMessages()
    {
      this.restService.getData('get-task-messages/' + this.task.id_task)
        .then((result)=>{
          this.response = result;
          if (this.response.error) {
              this.presentToast(this.response.text);
          } else {
              this.messages = this.response.messages;
          }
      });
    }

    public deleteMessage(scope)
    {
      console.log(scope);
    }
}
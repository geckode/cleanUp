import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonTextAvatar } from 'ionic-text-avatar';
import { HomePage } from './home';

@NgModule({
  declarations: [
    HomePage,
    IonTextAvatar
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
  ],
})
export class HomePageModule {}

import { Component } from '@angular/core';
import { NavController, PopoverController, LoadingController, ToastController, ModalController, ActionSheetController, Platform  } from 'ionic-angular';

import { HomePopoverPage } from '../home-popover/home-popover';
import { SettingPage } from '../setting/setting';
import { ListPage } from '../list/list';
import { CalendarPage } from '../calendar/calendar';
import { NewListModalPage } from '../new-list-modal/new-list-modal';
import { RestProvider } from '../../providers/rest/rest';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public user:any;
  public lists:any;
  public response:any;

  constructor(
    public navCtrl: NavController, 
    public popoverCtrl: PopoverController,
    public loadingCtrl: LoadingController,
    public restService: RestProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController,
    public plt: Platform) 
  {
    const data = JSON.parse(localStorage.getItem('userData'));
    this.user = data;
    this.getListUser();
  }

  //[Link to Views]
  public gotoSettings()
  { 
    this.navCtrl.push(SettingPage);
  }

  public showNewListModal() 
  {
    var id = this.user.id;
    let modal = this.modalCtrl.create(NewListModalPage,{idUser:id});
    modal.present();
  }

  public getListUser() 
  { 
    let loader = this.loadingCtrl.create({
      content: "Wait..."
    });
    this.restService.getData('get-lists/' + this.user.id)
      .then((result)=>{
        this.response = result;
        if (this.response.error){
          loader.dismiss();
          this.presentToast(this.response.text);
        } else {
          loader.dismiss();
          this.lists = this.response.userLists;
        }
      });
  }

  public goToList(id_list)
  {
    this.navCtrl.push(ListPage, {id_list: id_list});
  }

  public deleteList(id_list)
  {
	let loader = this.loadingCtrl.create({
          content: "Wait..."
      }); 
      loader.present();
      this.restService.getData('destroy-list/' + id_list)
          .then((result)=>{
            this.response = result;
            loader.dismiss();
            this.presentToast(this.response.text);
            this.getListUser();
      });
  }

  public presentPopover(myEvent)
  {
    let popover = this.popoverCtrl.create(HomePopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  public presentToast(msg) 
  {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  public doRefresh(refresher) 
  {
    this.getListUser();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 1000);
  }

  public presentActionSheet(id_list, nameList) {
     let actionSheet = this.actionSheetCtrl.create({
       title: nameList + ' Options',
       buttons: [
         {
           text: 'Print List',
           icon: !this.plt.is('ios') ? 'print' : null,
           handler: () => {
             console.log('Destructive clicked');
           }
         },
         {
           text: 'Show Calendar',
           icon: !this.plt.is('ios') ? 'calendar' : null,
           handler: () => {
              this.presentCalendar(id_list)
           }
         },
         {
           text: 'Share List',
           icon: !this.plt.is('ios') ? 'mail' : null,
           handler: () => {
             console.log('Archive clicked');
           }
         },
         {
           text: 'Delete',
           icon: !this.plt.is('ios') ? 'trash' : null,
           role: 'destructive',
           handler: () => {
           		this.deleteList(id_list);
           }
         },
         {
           text: 'Cancel',
           role: 'cancel',
           handler: () => {
             console.log('Cancel clicked');
           }
         }
       ]
     });

     actionSheet.present();
  }

  public presentCalendar(id_list)
  {
    this.navCtrl.push(CalendarPage, {id_list: id_list});
  }
}

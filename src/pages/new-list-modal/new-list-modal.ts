import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-new-list-modal',
  templateUrl: 'new-list-modal.html',
})
export class NewListModalPage {

	public response:any;
  	public users:any;
  	public list = {"name":"","idUser":"", "members": {}};
  	public items:any;

  	constructor(
  		public navCtrl: NavController,
  		public viewCtrl: ViewController,
  		public restService: RestProvider,
  		public toastCtrl: ToastController,
  		public navParams: NavParams) 
  	{
  		this.list.idUser = navParams.get('idUser');
    	this.getUsers();
  	}

  	public close() 
  	{
    	this.viewCtrl.dismiss();
  	}

  	public createNewList()
  	{
	  	this.restService.postData(this.list, 'new-list')
	      .then((result)=>{
	        this.response = result;
	        if (this.response.error){
	        	this.presentToast(this.response.text);
	        } else {
	            this.close();
				this.navCtrl.setRoot(TabsPage);
	        }
	      }, (err)=>{
	    });
  	}

  	public getUsers()
  	{
	    this.restService.getData('get-users/' + this.list.idUser)
	      .then((result)=>{
	        this.response = result;
	        if (this.response.error){
	        	this.presentToast(this.response.text);
	        } else {
	          this.users = this.response.users;
	        }
	    });
  	}

  	public assingToCurrentList(idUser, event) 
  	{

	    if (event.target.classList.contains('circle-button') || event.target.classList.contains('container-button')){
	      return alert('Error while select a member, try again');
	    }

	    var firstParent = event.target.parentElement;
	    var elementSelect = firstParent.parentElement;

	    if(typeof this.list.members[idUser] === 'undefined') {
	      this.list.members[idUser] = idUser;
	      elementSelect.classList.add('active');
	    } else {
	      if(elementSelect.classList.contains('active')){
	        elementSelect.classList.remove('active')
	      }
	      delete this.list.members[idUser];
	    }
	}

	public getItemsToSearch(ev) 
	{
	    var val = ev.target.value;
	    if (val && val.trim() != '') {
	      this.users = this.users.filter((item) => {
	        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
	      })
	    } else {
	    	this.getUsers();
	    }
	}	

  	public presentToast(msg) 
	{
	    let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 3000,
	      position: 'bottom'
	    });

	    toast.onDidDismiss(() => {
	      console.log('Dismissed toast');
	    });

	    toast.present();
	}


}
